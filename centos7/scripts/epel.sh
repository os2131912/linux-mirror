#!/bin/bash

BASE="/repos"

echo "# EPEL"

yum install epel-release -y

# epel
mkdir -p ${BASE}/download.fedoraproject.org/pub/epel/7/x86_64/
reposync --download-metadata -n -l -m -d --norepopath -r 'epel' -p ${BASE}/download.fedoraproject.org/pub/epel/7/x86_64/
createrepo -g comps.xml ${BASE}/download.fedoraproject.org/pub/epel/7/x86_64/
