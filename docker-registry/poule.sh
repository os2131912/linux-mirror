#!/bin/bash
echo "Début du tirage des images"
while read i; do
    echo "Tirage de \"$i\""
    docker rmi -f $(docker images -a -q)
    docker pull "$i"
    clear
done < "images-list.txt"
