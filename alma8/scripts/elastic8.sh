#!/bin/bash
set -e
# Repo-id            : elasticsearch-8.X
 

BASE="/repos"

echo "# ELASTIC 8"

mkdir -p ${BASE}/artifacts.elastic.co/packages/8.x/yum

curl -s https://artifacts.elastic.co/GPG-KEY-elasticsearch -o ${BASE}/artifacts.elastic.co/GPG-KEY-elasticsearch

rpm --import https://artifacts.elastic.co/GPG-KEY-elasticsearch

tee /etc/yum.repos.d/elasticsearch8.repo > /dev/null <<EOT
[elasticsearch-8.x]
name=Elasticsearch repository for 8.x packages
baseurl=https://artifacts.elastic.co/packages/8.x/yum
gpgcheck=1
gpgkey=https://artifacts.elastic.co/GPG-KEY-elasticsearch
enabled=0
autorefresh=1
type=rpm-md
EOT

reposync  --download-metadata --newest-only --downloadcomps --delete --remote-time --norepopath --repo  'elasticsearch-8.x' -p ${BASE}/artifacts.elastic.co/packages/8.x/yum
createrepo ${BASE}/artifacts.elastic.co/packages/8.x/yum
 