#!/bin/bash

BASE="/repos"

echo "# EPEL ALMA 8"

yum install epel-release -y

# epel
mkdir -p ${BASE}/download.fedoraproject.org/pub/epel/8/Everything/x86_64/
reposync --download-metadata --newest-only --downloadcomps --delete --remote-time --norepopath --repo 'epel' -p ${BASE}/download.fedoraproject.org/pub/epel/8/Everything/x86_64/
createrepo -g comps.xml ${BASE}/download.fedoraproject.org/pub/epel/8/Everything/x86_64/

# epel-modular
mkdir -p ${BASE}/download.fedoraproject.org/pub/epel/8/Modular/x86_64/
reposync --download-metadata --newest-only --downloadcomps --delete --remote-time --norepopath --repo 'epel-modular' -p ${BASE}/download.fedoraproject.org/pub/epel/8/Modular/x86_64/
createrepo -g comps.xml ${BASE}/download.fedoraproject.org/pub/epel/8/Modular/x86_64/
