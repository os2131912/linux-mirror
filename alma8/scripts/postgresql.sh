#!/bin/bash

BASE="/repos"

echo "# POSTGRESQL RHEL 8"
# https://packages.graylog2.org/repo/packages/graylog-4.2-repository_latest.rpm
dnf install -y https://download.postgresql.org/pub/repos/yum/reporpms/EL-8-x86_64/pgdg-redhat-repo-latest.noarch.rpm

mkdir -p ${BASE}/download.postgresql.org/pub/repos/yum/reporpms/EL-8-x86_64/
mkdir -p ${BASE}/download.postgresql.org/pub/repos/yum/common/redhat/rhel-7-x86_64/
mkdir -p ${BASE}/download.postgresql.org/pub/repos/yum/15/redhat/rhel-8-x86_64/
mkdir -p ${BASE}/download.postgresql.org/pub/repos/yum/14/redhat/rhel-8-x86_64/
mkdir -p ${BASE}/download.postgresql.org/pub/repos/yum/13/redhat/rhel-8-x86_64/
mkdir -p ${BASE}/download.postgresql.org/pub/repos/yum/12/redhat/rhel-8-x86_64/
mkdir -p ${BASE}/download.postgresql.org/pub/repos/yum/11/redhat/rhel-8-x86_64/
mkdir -p ${BASE}/download.postgresql.org/pub/repos/yum/10/redhat/rhel-8-x86_64/

curl -s http://download.postgresql.org/pub/repos/yum/RPM-GPG-KEY-PGDG    -o ${BASE}/download.postgresql.org/pub/repos/yum/RPM-GPG-KEY-PGDG   
curl -s http://download.postgresql.org/pub/repos/yum/RPM-GPG-KEY-PGDG-10 -o ${BASE}/download.postgresql.org/pub/repos/yum/RPM-GPG-KEY-PGDG-10
curl -s http://download.postgresql.org/pub/repos/yum/RPM-GPG-KEY-PGDG-11 -o ${BASE}/download.postgresql.org/pub/repos/yum/RPM-GPG-KEY-PGDG-11
curl -s http://download.postgresql.org/pub/repos/yum/RPM-GPG-KEY-PGDG-12 -o ${BASE}/download.postgresql.org/pub/repos/yum/RPM-GPG-KEY-PGDG-12
curl -s http://download.postgresql.org/pub/repos/yum/RPM-GPG-KEY-PGDG-13 -o ${BASE}/download.postgresql.org/pub/repos/yum/RPM-GPG-KEY-PGDG-13
curl -s http://download.postgresql.org/pub/repos/yum/RPM-GPG-KEY-PGDG-14 -o ${BASE}/download.postgresql.org/pub/repos/yum/RPM-GPG-KEY-PGDG-14
curl -s http://download.postgresql.org/pub/repos/yum/RPM-GPG-KEY-PGDG-15 -o ${BASE}/download.postgresql.org/pub/repos/yum/RPM-GPG-KEY-PGDG-15
curl -s http://download.postgresql.org/pub/repos/yum/RPM-GPG-KEY-PGDG-84 -o ${BASE}/download.postgresql.org/pub/repos/yum/RPM-GPG-KEY-PGDG-84
curl -s http://download.postgresql.org/pub/repos/yum/RPM-GPG-KEY-PGDG-90 -o ${BASE}/download.postgresql.org/pub/repos/yum/RPM-GPG-KEY-PGDG-90
curl -s http://download.postgresql.org/pub/repos/yum/RPM-GPG-KEY-PGDG-91 -o ${BASE}/download.postgresql.org/pub/repos/yum/RPM-GPG-KEY-PGDG-91
curl -s http://download.postgresql.org/pub/repos/yum/RPM-GPG-KEY-PGDG-92 -o ${BASE}/download.postgresql.org/pub/repos/yum/RPM-GPG-KEY-PGDG-92
curl -s http://download.postgresql.org/pub/repos/yum/RPM-GPG-KEY-PGDG-93 -o ${BASE}/download.postgresql.org/pub/repos/yum/RPM-GPG-KEY-PGDG-93
curl -s http://download.postgresql.org/pub/repos/yum/RPM-GPG-KEY-PGDG-94 -o ${BASE}/download.postgresql.org/pub/repos/yum/RPM-GPG-KEY-PGDG-94
curl -s http://download.postgresql.org/pub/repos/yum/RPM-GPG-KEY-PGDG-95 -o ${BASE}/download.postgresql.org/pub/repos/yum/RPM-GPG-KEY-PGDG-95
curl -s http://download.postgresql.org/pub/repos/yum/RPM-GPG-KEY-PGDG-96 -o ${BASE}/download.postgresql.org/pub/repos/yum/RPM-GPG-KEY-PGDG-96

# accepte les clés GPG avant le reposync
yum makecache -y

curl -s https://download.postgresql.org/pub/repos/yum/reporpms/EL-8-x86_64/pgdg-redhat-repo-latest.noarch.rpm -o ${BASE}/download.postgresql.org/pub/repos/yum/reporpms/EL-8-x86_64/pgdg-redhat-repo-latest.noarch.rpm

reposync --download-metadata --downloadcomps --delete --remote-time --norepopath --repo 'pgdg-common' -p ${BASE}/download.postgresql.org/pub/repos/yum/common/redhat/rhel-8-x86_64/
reposync --download-metadata --downloadcomps --delete --remote-time --norepopath --repo 'pgdg10'      -p ${BASE}/download.postgresql.org/pub/repos/yum/10/redhat/rhel-8-x86_64/
reposync --download-metadata --downloadcomps --delete --remote-time --norepopath --repo 'pgdg11'      -p ${BASE}/download.postgresql.org/pub/repos/yum/11/redhat/rhel-8-x86_64/
reposync --download-metadata --downloadcomps --delete --remote-time --norepopath --repo 'pgdg12'      -p ${BASE}/download.postgresql.org/pub/repos/yum/12/redhat/rhel-8-x86_64/
reposync --download-metadata --downloadcomps --delete --remote-time --norepopath --repo 'pgdg13'      -p ${BASE}/download.postgresql.org/pub/repos/yum/13/redhat/rhel-8-x86_64/
reposync --download-metadata --downloadcomps --delete --remote-time --norepopath --repo 'pgdg14'      -p ${BASE}/download.postgresql.org/pub/repos/yum/14/redhat/rhel-8-x86_64/
reposync --download-metadata --downloadcomps --delete --remote-time --norepopath --repo 'pgdg15'      -p ${BASE}/download.postgresql.org/pub/repos/yum/15/redhat/rhel-8-x86_64/

createrepo ${BASE}/download.postgresql.org/pub/repos/yum/common/redhat/rhel-8-x86
createrepo ${BASE}/download.postgresql.org/pub/repos/yum/10/redhat/rhel-8-x86_64/
createrepo ${BASE}/download.postgresql.org/pub/repos/yum/11/redhat/rhel-8-x86_64/
createrepo ${BASE}/download.postgresql.org/pub/repos/yum/12/redhat/rhel-8-x86_64/
createrepo ${BASE}/download.postgresql.org/pub/repos/yum/13/redhat/rhel-8-x86_64/
createrepo ${BASE}/download.postgresql.org/pub/repos/yum/14/redhat/rhel-8-x86_64/
createrepo ${BASE}/download.postgresql.org/pub/repos/yum/15/redhat/rhel-8-x86_64/
